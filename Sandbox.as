package 
{
	import campaign.Data_300x250;
	import campaign.Styles_300x250;
	import com.workfunc.Bananas;

	import flash.events.MouseEvent;

	public class Sandbox extends Bananas 
	{
		public function Sandbox() 
		{
			super(this, 300, 250, Data_300x250.json, Styles_300x250.css);
		}

		override public function init():void 
		{
			super.init();

			trace("\n> Banner has been initiated.");
		}

		public function frame1():void 
		{
			trace("\nframe1 added.");
		}

		public function frame2():void 
		{
			trace("\nframe2 added.");
		}

		public function frame3():void 
		{
			trace("\nframe3 added.");
		}

		public function frame4():void 
		{
			trace("\nframe4 added.");
		}

		override public function clickTagHandler($e:MouseEvent):void 
		{
			trace("\n> The banner's clickTag works.");
		}
	}
}