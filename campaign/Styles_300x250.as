package campaign  
{
	import com.workfunc.data.BananaStyle;

	import flash.text.StyleSheet;

	public class Styles_300x250 extends BananaStyle 
	{
		public static function get css():StyleSheet 
		{
			BananaStyle.styles.ebc = 
			{
				fontFamily: new GillSansMT().fontName, 
				kerning: 0
			};

			BananaStyle.styles.ribbonRedText = 
			{
				color: "#cd1f27", 
				fontSize: 20
			};

			BananaStyle.styles.whiteText29 = 
			{
				color: "#FFFFFF", 
				fontSize: 29
			};

			BananaStyle.styles.whiteText30 = 
			{
				color: "#FFFFFF", 
				fontSize: 30
			};

			BananaStyle.styles.whiteText32 = 
			{
				color: "#FFFFFF", 
				fontSize: 32, 
				textAlign: "center"
			};

			BananaStyle.styles.whiteText38 = 
			{
				color: "#FFFFFF", 
				fontSize: 38, 
				leading: -8, 
				textAlign: "center"
			};

			BananaStyle.styles.whiteText42 = 
			{
				color: "#FFFFFF", 
				fontSize: 42
			};

			BananaStyle.styles.whiteText43 = 
			{
				color: "#FFFFFF", 
				fontSize: 43
			};

			BananaStyle.styles.whiteText44 = 
			{
				color: "#FFFFFF", 
				fontSize: 44, 
				leading: -8, 
				textAlign: "center"
			};

			BananaStyle.styles.whiteText53 = 
			{
				color: "#FFFFFF", 
				fontSize: 53
			};

			BananaStyle.styles.asterisk = 
			{
				color: "#FFFFFF", 
				fontSize: 20
			};

			BananaStyle.styles.legal = 
			{
				fontFamily: new GillSansMT().fontName, 
				color: "#FFFFFF", 
				fontSize: 11, 
				kerning: 0, 
				textAlign: "center"
			};

			// Berthold Extra Bold Condensed
			BananaStyle.customCss(".frame1_pre_leftRibbon_300", [BananaStyle.styles.ebc, BananaStyle.styles.ribbonRedText]);
			BananaStyle.customCss(".frame1_pre_w29_300", [BananaStyle.styles.ebc, BananaStyle.styles.whiteText29]);
			BananaStyle.customCss(".frame1_pre_w30_300", [BananaStyle.styles.ebc, BananaStyle.styles.whiteText30]);
			BananaStyle.customCss(".frame1_pre_w32_300", [BananaStyle.styles.ebc, BananaStyle.styles.whiteText32]);
			BananaStyle.customCss(".frame1_pre_w38_300", [BananaStyle.styles.ebc, BananaStyle.styles.whiteText38]);
			BananaStyle.customCss(".frame1_pre_w42_300", [BananaStyle.styles.ebc, BananaStyle.styles.whiteText42]);
			BananaStyle.customCss(".frame1_pre_w43_300", [BananaStyle.styles.ebc, BananaStyle.styles.whiteText43]);
			BananaStyle.customCss(".frame1_pre_w44_300", [BananaStyle.styles.ebc, BananaStyle.styles.whiteText44]);
			BananaStyle.customCss(".frame1_pre_w53_300", [BananaStyle.styles.ebc, BananaStyle.styles.whiteText53]);
			BananaStyle.customCss(".asterisk", [BananaStyle.styles.ebc, BananaStyle.styles.asterisk]);
			// Berthold Regular
			BananaStyle.customCss(".legal", BananaStyle.styles.legal);

			return BananaStyle.css;
		}
	}
}