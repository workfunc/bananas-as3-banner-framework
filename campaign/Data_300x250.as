package campaign 
{
	public class Data_300x250 
	{
		public static function get json():Object 
		{
			var json:Object = 
			{
				env: "development", 
				banner: 
				{
					width: 300, 
					height: 250, 
					loopAmount: 1, 
					border: "0x000000", 
					background: 
					{
						x: 0, 
						y: 0
					}, 
					frames: 
					[
						/*
						 * 300x250 Frame 1
						 */
						{
							time: 3.5, 
							graphics: 
							[
								{
									name: "_logo", 
									animation: 
									{
										origin: 
										{
											x: 150, 
											y: 33
										}
									}
								}, 
								{
									name: "_f1_l3_container", 
									animation: 
									{
										origin: 
										{
											x: -250, 
											y: 147, 
											alpha: 0
										}, 
										start: 
										{
											x: 180, 
											alpha: 1
										}
									}
								}, 
								{
									name: "_rightRibbonContainer", 
									animation: 
									{
										origin: 
										{
											x: 300, 
											y: 188
										}, 
										start: 
										{
											x: 120
										}
									}
								}, 
								{
									name: "_rightRibbon", 
									animation: 
									{
										origin: 
										{
											x: 0, 
											y: 0
										}
									}
								}, 
								{
									name: "_giftcard", 
									animation: 
									{
										origin: 
										{
											x: 173, 
											y: 114, 
											scaleX: 0, 
											scaleY: 0, 
											alpha: 0
										}, 
										start: 
										{
											scaleX: 1, 
											scaleY: 1, 
											alpha: 1
										}
									}
								}, 
								{
									name: "_refresh", 
									animation: 
									{
										origin: 
										{
											x: 288, 
											y: 10
										}
									}
								}
							], 
							frametext: 
							[
								{
									text: "OBTÉN", 
									cssClass: "frame1_pre_w32_300", 
									animation: 
									{
										origin: 
										{
											x: -200, 
											y: 84, 
											alpha: 0
										}, 
										start: 
										{
											x: 53, 
											alpha: 1
										}
									}
								}, 
								{
									text: "EN", 
									cssClass: "frame1_pre_w32_300", 
									animation: 
									{
										origin: 
										{
											x: 300, 
											y: 84, 
											alpha: 0
										}, 
										start: 
										{
											x: 206, 
											alpha: 1
										}
									}
								}, 
								{
									text: "UNA TARJETA", 
									cssClass: "frame1_pre_w38_300", 
									animation: 
									{
										origin: 
										{
											x: 400, 
											y: 113, 
											alpha: 0
										}, 
										start: 
										{
											x: 49, 
											alpha: 1
										}
									}
								}, 
								{
									text: "DE", 
									cssClass: "frame1_pre_w29_300", 
									animation: 
									{
										origin: 
										{
											x: 0, 
											y: 3
										}
									}
								}, 
								{
									text: "RECOMPENSA", 
									cssClass: "frame1_pre_w38_300", 
									animation: 
									{
										origin: 
										{
											x: 35, 
											y: 0
										}
									}
								}, 
								{
									text: "¡SÓLO HOY!", 
									cssClass: "frame1_pre_leftRibbon_300", 
									animation: 
									{
										origin: 
										{
											x: 170, 
											y: 3, 
											alpha: 0
										}, 
										start: 
										{
											alpha: 1
										}
									}
								}, 
								{
									text: "Con activación del servicio de DishLATINO.<br>Disponible 2 de dic. de 6:30am a 10:30pm MST.", 
									cssClass: "legal", 
									animation: 
									{
										origin: 
										{
											x: 150, 
											y: 235, 
											alpha: 0
										}, 
										start: 
										{
											alpha: 0.8
										}
									}
								}
							]
						}, 
						/* 
						 * 300x250 Frame 2
						 */
						{
							time: 2.6, 
							graphics: 
							[
								{
									name: "_package", 
									animation: 
									{
										origin: 
										{
											x: 300, 
											y: 63
										}, 
										start: 
										{
											x: 192
										}
									}
								}
							], 
							frametext: 
							[
								{
									text: "CON TU", 
									cssClass: "frame1_pre_w42_300", 
									animation: 
									{
										origin: 
										{
											x: -200, 
											y: 71, 
											alpha: 0
										}, 
										start: 
										{
											x: 63, 
											alpha: 1
										}
									}
								}, 
								{
									text: "PAQUETE", 
									cssClass: "frame1_pre_w53_300", 
									animation: 
									{
										origin: 
										{
											x: 300, 
											y: 108, 
											alpha: 0
										}, 
										start: 
										{
											x: 63, 
											alpha: 1
										}
									}
								}, 
								{
									text: "PREFERIDO", 
									cssClass: "frame1_pre_w43_300", 
									animation: 
									{
										origin: 
										{
											x: -200, 
											y: 158, 
											alpha: 0
										}, 
										start: 
										{
											x: 63, 
											alpha: 1
										}
									}
								}, 
								{
									text: "DE DishLATINO", 
									cssClass: "frame1_pre_w32_300", 
									animation: 
									{
										origin: 
										{
											x: 300, 
											y: 197, 
											alpha: 0
										}, 
										start: 
										{
											x: 63, 
											alpha: 1
										}
									}
								}
							]
						}, 
						/* 
						 * 300x250 Frame 3
						 */
						{
							time: 3.2, 
							graphics: 
							[
								{
									name: "_tv", 
									animation: 
									{
										origin: 
										{
											x: 43, 
											y: 74, 
											alpha: 0
										}, 
										start: 
										{
											alpha: 1
										}
									}
								}, 
								{
									name: "_frameThreeRibbonContainer", 
									animation: 
									{
										origin: 
										{
											x: 57, 
											y: 83
										}
									}
								}
							], 
							frametext: 
							[
								{
									text: "ADEMÁS", 
									cssClass: "frame1_pre_w30_300", 
									animation: 
									{
										origin: 
										{
											x: 100, 
											y: 20, 
											alpha: 0, 
											scaleX: 0, 
											scaleY: 0
										}, 
										start: 
										{
											scaleX: 1, 
											scaleY: 1, 
											alpha: 1
										}
									}
								}, 
								{
									text: "HD GRATIS", 
									cssClass: "frame1_pre_w44_300", 
									animation: 
									{
										origin: 
										{
											x: 95, 
											y: 50, 
											alpha: 0, 
											scaleX: 0, 
											scaleY: 0
										}, 
										start: 
										{
											scaleX: 1, 
											scaleY: 1, 
											alpha: 1
										}
									}
								}, 
								{
									text: "DE POR VIDA", 
									cssClass: "frame1_pre_w38_300", 
									animation: 
									{
										origin: 
										{
											x: 95, 
											y: 85, 
											alpha: 0, 
											scaleX: 0, 
											scaleY: 0
										}, 
										start: 
										{
											scaleX: 1, 
											scaleY: 1, 
											alpha: 1
										}
									}
								}, 
								{
									text: "*", 
									cssClass: "asterisk", 
									animation: 
									{
										origin: 
										{
											x: 191, 
											y: 77, 
											alpha: 0, 
											scaleX: 0, 
											scaleY: 0
										}, 
										start: 
										{
											scaleX: 1, 
											scaleY: 1, 
											alpha: 1
										}
									}
								}, 
								{
									text: "*Requiere contrato de 24 meses y calificación de crédito.<br>Con paquetes selectos.", 
									cssClass: "legal", 
									animation: 
									{
										origin: 
										{
											x: 150, 
											y: 234, 
											alpha: 0
										}, 
										start: 
										{
											alpha: 0.8
										}
									}
								}
							]
						}, 
						/* 
						 * 300x250 Frame 4
						 */
						{
							time: 3.2, 
							graphics: 
							[
								{
									name: "_frameFourRibbonContainer", 
									animation: 
									{
										origin: 
										{
											x: -100, 
											y: 65
										}, 
										start: 
										{
											x: 130
										}
									}
								}, 
								{
									name: "_leftRibbon", 
									animation: 
									{
										origin: 
										{
											x: 0, 
											y: 0, 
											scaleX: -1
										}
									}
								}, 
								{
									name: "_rightRibbonContainer", 
									animation: 
									{
										origin: 
										{
											x: 90, 
											y: 188
										}, 
										start: 
										{
											x: 120
										}
									}
								}, 
								{
									name: "_rightRibbon", 
									animation: 
									{
										origin: 
										{
											x: 0, 
											y: 0
										}
									}
								}
							], 
							frametext: 
							[
								{
									text: "SÓLO HOY", 
									cssClass: "frame1_pre_leftRibbon_300", 
									animation: 
									{
										origin: 
										{
											x: 15, 
											y: 3
										}
									}
								}, 
								{
									text: "¡SUSCRÍBETE YA!", 
									cssClass: "frame1_pre_leftRibbon_300", 
									animation: 
									{
										origin: 
										{
											x: 170, 
											y: 3, 
											alpha: 0
										}, 
										start: 
										{
											alpha: 1
										}
									}
								}, 
								{
									text: "Disponible 2 de dic. de 6:30am a 10:30pm MST.", 
									cssClass: "legal", 
									animation: 
									{
										origin: 
										{
											x: 150, 
											y: 242, 
											alpha: 0
										}, 
										start: 
										{
											alpha: 0.8
										}
									}
								}
							]
						}
					]
				}
			};
			return json;
		}
	}
}