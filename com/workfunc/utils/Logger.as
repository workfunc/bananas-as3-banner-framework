package com.workfunc.utils 
{
	import com.workfunc.data.Model;

	import flash.display.DisplayObject;
	import flash.external.ExternalInterface;
	
	public class Logger 
	{
		public static var 
			enabled						:Boolean = 					true;

		public static function log(...args):void 
		{
			if(Model.environment == "development") 
			{
				if(enabled) 
				{
					if(ExternalInterface.available) 
					{
						try 
						{
							ExternalInterface.call("console.log", args);
						}
						catch(e:Error) { }
					}
					trace.apply(null, args);
				}
			}
		}

		public static function returnDisplayList($display:*):void 
		{
			for(var i:uint = 0; i < $display.numChildren; i++) 
			{
				Logger.log('\t|\t ' +i+'.\t name:' + $display.getChildAt(i).name + '\t type:' + typeof ($display.getChildAt(i))+ '\t' + $display.getChildAt(i));
			}
		}
	}
}