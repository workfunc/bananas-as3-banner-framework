package com.workfunc.effects 
{
	import com.workfunc.core.StaticBase;
	import com.workfunc.events.BananasEvent;

	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Matrix;
	import flash.utils.Timer;
	import flash.events.TimerEvent;

	public class PixelCrusher extends StaticBase 
	{
		private static var 
			_src 							:BitmapData, 
			_bmp							:Bitmap, 
			_pixelSize				:int, 
			_holder						:*, 
			_timer 						:Timer;

		public static function sample($source:BitmapData, $holder:*):void 
		{
			_holder = $holder;
			removeChildren(_holder);

			var bmp:Bitmap = new Bitmap($source);

			_holder.addChild(bmp);
		}

		public static function pixelate($source:BitmapData, $pixelSize:int, $holder:*):void 
		{
			_holder = $holder;
			removeChildren(_holder);

			var 
				bitmap 				:Bitmap = 				new Bitmap($source), 
				orig					:BitmapData = 		new BitmapData($source.width / $pixelSize, $source.height / $pixelSize), 
				matrix 				:Matrix = 				new Matrix;

			bitmap.smoothing = true;
			matrix.scale(1 / $pixelSize, 1 / $pixelSize);
			orig.draw(bitmap, matrix);

			bitmap = new Bitmap(orig);
			
			var pixelBlock:BitmapData = new BitmapData($source.width, $source.height);
			matrix.invert();
			pixelBlock.draw(bitmap, matrix);
			orig.dispose();

			_bmp = null;
			_bmp = new Bitmap(pixelBlock);
			_bmp.width = $source.width;
			_bmp.height = $source.height;
			_holder.addChild(_bmp);
		}

		public static function removeChildren($mc:*):void 
		{
			if($mc.numChildren != 0) 
			{
				var k:int = $mc.numChildren;

				while(k--) 
				{
					$mc.removeChildAt(k);
				}
			}
		}

		public static function crush($source:BitmapData, $holder:*, $pixelSize:int, $seconds:Number):void 
		{
			_src = $source;
			_pixelSize = $pixelSize;
			_holder = $holder;

			_timer = new Timer($seconds * 1000);
			_timer.addEventListener(TimerEvent.TIMER, _degrade);
			_timer.start();
		}

		private static function _degrade($e:TimerEvent):void 
		{
			pixelate(_src, _pixelSize++, _holder);

			if(_pixelSize >= 14) 
			{
				_timer.stop();
				_timer.removeEventListener(TimerEvent.TIMER, _degrade);
				
				_holder.dispatchEvent(new BananasEvent(BananasEvent.ANIMATION_COMPLETE));
			}
		}
	}
}