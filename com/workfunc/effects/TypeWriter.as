package com.workfunc.effects 
{
	import com.workfunc.core.StaticBase;
	import com.workfunc.events.BananasEvent;
	import com.workfunc.display.TextLine;

	import flash.utils.Timer;
	import flash.events.TimerEvent;

	public class TypeWriter extends StaticBase 
	{
		private static var 
			_textfield						:TextLine = 					new TextLine, 
			_tempText 						:TextLine = 					new TextLine, 
			_counter							:int, 
			_text									:String, 
			_message							:String = 						"", 
			_timer 								:Timer;

		public static function type($stage:*, $textfield:TextLine, $speed:Number, $text:String):void 
		{
			//_text = $text;
			_tempText.htmlText = $text;
			_textfield = $textfield;

			_timer = new Timer($speed * 1000);
			_timer.addEventListener(TimerEvent.TIMER, _type);
			_timer.start();
		}

		private static function _type($e:TimerEvent):void 
		{
			var character:String = _text.charAt(_counter);

			if(_message == null || _message == "") _message = character;
			else _message = _message + character;

			_textfield.appendText(character);
			trace(_textfield.text);
			_counter++;

			if(_counter == _text.length) 
			{
				_timer.stop();
				_timer.removeEventListener(TimerEvent.TIMER, _type);
				
				_textfield.dispatchEvent(new BananasEvent(BananasEvent.ANIMATION_COMPLETE));
			}
		}
	}
}