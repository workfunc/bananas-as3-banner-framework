package com.workfunc.effects 
{
	import com.workfunc.core.StaticBase;
	import com.greensock.TweenLite;
	import com.greensock.easing.*;

	public class Animation extends StaticBase 
	{
		public static function pulse($clip:*, $amount:int, $speed:Number, $easeType:*, $delay:Number = 0):void 
		{
			var 
				counter 		:int = 			0, 
				func 				:Function = function():void 
				{
					TweenLite.to($clip, $speed, {scaleX: 0.5, scaleY: 0.5, ease: $easeType});
					TweenLite.to($clip, $speed, {scaleX: 1, scaleY: 1, delay: ($speed + $delay), ease: $easeType, onComplete: function():void { counter++; }});
				};

			if(counter < $amount) func();
		}
	}
}