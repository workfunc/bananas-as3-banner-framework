package com.workfunc.components 
{
	import flash.display.Shape;

	public class Draw extends Shape 
	{
		public static function rect($params:Object):Shape 
		{
			if($params.color == undefined) $params.color = 0x00FFFF;
			if($params.alpha == undefined) $params.alpha = 1;

			var s:Shape = new Shape();
				s.graphics.beginFill($params.color, $params.alpha);
				(!$params.rounded) ? s.graphics.drawRect($params.x, $params.y, $params.width, $params.height) : s.graphics.drawRoundRect($params.x, $params.y, $params.width, $params.height, $params.cornerRadius);
				s.graphics.endFill();

			return s;
		}

		public static function border($params:Object):Shape 
		{
			if($params.thickness == undefined) $params.thickness = 1;
			if($params.color == undefined) $params.color = 0x000000;
			if($params.alpha == undefined) $params.alpha = 1;

			var b:Shape = new Shape();
				b.graphics.lineStyle($params.thickness, $params.color, $params.alpha);
				b.graphics.moveTo(0.5, 0.5);
				b.graphics.lineTo(($params.width - 0.5), 0);
				b.graphics.lineTo(($params.width - 0.5), ($params.height - 0.5));
				b.graphics.lineTo(0.5, ($params.height - 0.5));
				b.graphics.lineTo(0.5, 0.5);

			return b;
		}
	}
}