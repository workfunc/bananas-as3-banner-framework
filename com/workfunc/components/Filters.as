package com.workfunc.components 
{
	import flash.filters.DropShadowFilter;
	import flash.filters.GlowFilter;
	import flash.filters.BlurFilter;

	public class Filters 
	{
		public static function dropShadow($params:Object):DropShadowFilter 
		{
			var params:Object = {};
			
			($params.distance == undefined) ? params.distance = 2 : params.distance = $params.distance;
			($params.angle == undefined) ? params.angle = 45 : params.angle = $params.angle;
			($params.color == undefined) ? params.color = 0x000000 : params.color = $params.color;
			($params.alpha == undefined) ? params.alpha = 0.5 : params.alpha = $params.alpha;
			($params.blurX == undefined) ? params.blurX = 4 : params.blurX = $params.blurX;
			($params.blurY == undefined) ? params.blurY = 4 : params.blurY = $params.blurY;
			($params.strength == undefined) ? params.strength = 1 : params.strength = $params.strength;
			($params.quality == undefined) ? params.quality = 3 : params.quality = $params.quality;
			($params.inner == undefined) ? params.inner = false : params.inner = $params.inner;
			($params.knockout == undefined) ? params.knockout = false : params.knockout = $params.knockout;
			($params.hideObject == undefined) ? params.hideObject = false : params.hideObject = $params.hideObject;

			var shadow:DropShadowFilter = new DropShadowFilter(params.distance, params.angle, params.color, params.alpha, params.blurX, params.blurY, params.strength, params.quality, params.inner, params.knockout, params.hideObject);

			return shadow;
		}

		public static function glow($params:Object):GlowFilter 
		{
			var params:Object = {};
			
			($params.color == undefined) ? params.color = 0xFFFF66 : params.color = $params.color;
			($params.alpha == undefined) ? params.alpha = 0.5 : params.alpha = $params.alpha;
			($params.blurX == undefined) ? params.blurX = 12 : params.blurX = $params.blurX;
			($params.blurY == undefined) ? params.blurY = 12 : params.blurY = $params.blurY;
			($params.strength == undefined) ? params.strength = 5 : params.strength = $params.strength;
			($params.quality == undefined) ? params.quality = 3 : params.quality = $params.quality;
			($params.inner == undefined) ? params.inner = false : params.inner = $params.inner;
			($params.knockout == undefined) ? params.knockout = false : params.knockout = $params.knockout;

			var glow:GlowFilter = new GlowFilter(params.color, params.alpha, params.blurX, params.blurY, params.strength, params.quality, params.inner, params.knockout);

			return glow;
		}

		public static function blur($params:Object):BlurFilter 
		{
			var params:Object = {};
			
			($params.blurX == undefined) ? params.blurX = 6 : params.blurX = $params.blurX;
			($params.blurY == undefined) ? params.blurY = 6 : params.blurY = $params.blurY;
			($params.quality == undefined) ? params.quality = 3 : params.quality = $params.quality;

			var blur:BlurFilter = new BlurFilter(params.blurX, params.blurY, params.quality);

			return blur;
		}
	}
}