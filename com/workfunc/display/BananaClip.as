package com.workfunc.display 
{
	import com.workfunc.components.Draw;

	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.events.Event;

	public class BananaClip extends MovieClip 
	{
		private var 
			_container							:*, 
			_properties							:Object = 							{}, 
			_boundingBox						:DisplayObject, 
			_foreground							:Boolean, 
			_aboveClickTag					:Boolean;

		public function BananaClip() 
		{
			super();

			_boundingBox = this.addChild(Draw.rect({x: 0, y: 0, width: 300, height: 250}));
			boundingBox(false);
		}

		public function afterAdd($e:Event = null):void 
		{
			(_properties.originValues.x == undefined) ? super.x = 0 : super.x = _properties.originValues.x;
			(_properties.originValues.y == undefined) ? super.y = 0 : super.y = _properties.originValues.y;
			(_properties.originValues.alpha == undefined) ? super.alpha = 1 : super.alpha = _properties.originValues.alpha;
			(_properties.originValues.scaleX == undefined) ? super.scaleX = 1 : super.scaleX = _properties.originValues.scaleX;
			(_properties.originValues.scaleY == undefined) ? super.scaleY = 1 : super.scaleY = _properties.originValues.scaleY;
			(_properties.originValues.rotation == undefined) ? super.rotation = 0 : super.rotation = _properties.originValues.rotation;
		}

		public function reset():void 
		{
			(_properties.originValues.x == undefined) ? super.x = 0 : super.x = _properties.originValues.x;
			(_properties.originValues.y == undefined) ? super.y = 0 : super.y = _properties.originValues.y;
			(_properties.originValues.alpha == undefined) ? super.alpha = 1 : super.alpha = _properties.originValues.alpha;
			(_properties.originValues.scaleX == undefined) ? super.scaleX = 1 : super.scaleX = _properties.originValues.scaleX;
			(_properties.originValues.scaleY == undefined) ? super.scaleY = 1 : super.scaleY = _properties.originValues.scaleY;
			(_properties.originValues.rotation == undefined) ? super.rotation = 0 : super.rotation = _properties.originValues.rotation;
		}

		public function boundingBox($state:Boolean):void 
		{
			if($state) 
			{
				_boundingBox.visible = true;
				_boundingBox.alpha = 1;
			} 
			else 
			{
				_boundingBox.visible = false;
				_boundingBox.alpha = 0;
			}
		}

		override public function get filters():Array 
    {
    	return this.filters;
    }

		override public function set filters(value:Array):void 
    {
    	this.filters = value;
    }

		public function set properties($data:Object):void 
		{
			_properties = $data;
		}

		public function get properties():Object 
		{
			return _properties;
		}

		public function get origin():Object 
		{
			return _properties.originValues;
		}

		public function get start():Object 
		{
			return _properties.startValues;
		}

		public function get end():Object 
		{
			return _properties.endValues;
		}

		public function set container($parent:*):void 
		{
			_container = $parent;
		}

		public function get container():* 
		{
			return _container;
		}

		public function set foreground($set:Boolean):void 
		{
			_foreground = $set;
		}

		public function get foreground():Boolean 
		{
			return _foreground;
		}

		public function set aboveTheClickTag($set:Boolean):void 
		{
			_aboveClickTag = $set;
		}

		public function get aboveTheClickTag():Boolean 
		{
			return _aboveClickTag;
		}
	}
}