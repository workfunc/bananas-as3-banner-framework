package com.workfunc.display 
{
	import com.workfunc.data.Model;

	import flash.events.Event;

	public class Clip extends BananaClip 
	{
		private var 
			_centerOffset:					Boolean;

		public function Clip() 
		{
			super();
		}

		public function set centerX($centerOffset:Boolean):void 
		{
			_centerOffset = $centerOffset;

			if(!_centerOffset) 
				(properties.originValues.x == undefined) ? super.x = 0 : super.x = properties.originValues.x;
			else 
				super.x = Model.bannerWidth * 0.5;
		}

		override public function afterAdd($e:Event = null):void 
		{
			super.afterAdd();

			if(!_centerOffset) 
				(properties.originValues.x == undefined) ? super.x = 0 : super.x = properties.originValues.x;
			
			super.removeEventListener(Event.ADDED, afterAdd, true);
		}
	}
}