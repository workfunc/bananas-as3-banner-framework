package com.workfunc.display 
{
	import com.workfunc.data.Model;

	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.AntiAliasType;
	import flash.text.StyleSheet;
	import flash.events.Event;

	public class TextLine extends BananaClip 
	{
		private var 
			_textfield						:TextField = 						new TextField, 
			_center								:Boolean, 
			_upper								:Boolean;

		public function TextLine() 
		{
			super();
		}

		override public function afterAdd($e:Event = null):void 
		{
			this.removeEventListener(Event.ADDED, afterAdd, true);

			super.afterAdd();
			_center = properties.center;
			_upper = properties.uppercase;

			_textfield.name = properties.name;
			_textfield.autoSize = TextFieldAutoSize.LEFT;
			_textfield.multiline = true;
			_textfield.selectable = false;
			_textfield.antiAliasType = AntiAliasType.ADVANCED;
			_textfield.styleSheet = Model.css;
			_textfield.embedFonts = true;

			try 
			{
				if(_upper) 
				{
					var temp:String = properties.text.toUpperCase();
					trace(temp);

					_textfield.htmlText = "<p class=\"" + properties.css + "\">" + temp + "</p>";
				} 
				else 
				{
					_textfield.htmlText = "<p class=\"" + properties.css + "\">" + properties.text + "</p>";
				}
			} 
			catch($e:Error) 
			{
				throw new Error("\n🍌 There is no Text or CSS Class that matches what you have provided.\n" + $e);
			}
			this.addChild(_textfield);

			if(_center) 
			{
				_textfield.x = -(_textfield.width * 0.5);
				_textfield.y = -(_textfield.height * 0.5);
			} 
			else 
			{
				_textfield.x = -(Model.bannerWidth * 0.5);
				_textfield.y = 0;
			}
			if(_textfield.width >= Model.bannerWidth) 
			{
				_textfield.width = Model.bannerWidth - _textfield.x;
				_textfield.wordWrap = true;
			}
		}

		override public function get x():Number 
		{
			return super.x + Model.bannerWidth * 0.5;
		}

		override public function set x(value:Number):void 
		{
			super.x = value + Model.bannerWidth * 0.5;
		}

		public function get antiAliasType():String 
		{
			return this._textfield.antiAliasType;
		}

    public function set antiAliasType(value:String):void 
    {
    	this._textfield.antiAliasType = value;
    }

    public function appendText(newText:String):void 
    {
    	this._textfield.htmlText = "<p class=\"" + properties.css + "\">" + newText + "</p>";
    }

    public function get autoSize():String 
    {
			return this._textfield.autoSize;
		}

		public function set autoSize(value:String):void 
		{
    	this._textfield.autoSize = value;
    }

    public function get center():Boolean 
		{
			return _center;
		}

		public function set center($c:Boolean):void 
		{
			_center = $c;
		}

    public function get embedFonts():Boolean 
		{
			return this._textfield.embedFonts;
		}

    public function set embedFonts(value:Boolean):void 
    {
    	this._textfield.embedFonts = value;
    }

    override public function get filters():Array 
    {
    	return this._textfield.filters;
    }

		override public function set filters(value:Array):void 
    {
    	this._textfield.filters = value;
    }

    public function get htmlText():String 
    {
			return this._textfield.htmlText;
		}

		public function set htmlText(value:String):void 
		{
			this._textfield.htmlText = value;
		}

    public function get multiline():Boolean 
		{
			return this._textfield.multiline;
		}

    public function set multiline(value:Boolean):void 
    {
    	this._textfield.multiline = value;
    }

    override public function get name():String 
		{
			return this._textfield.name;
		}

    override public function set name(value:String):void 
    {
    	this._textfield.name = value;
    }

    public function get selectable():Boolean 
		{
			return this._textfield.selectable;
		}

    public function set selectable(value:Boolean):void 
    {
    	this._textfield.selectable = value;
    }

    public function get styleSheet():StyleSheet 
    {
    	return this._textfield.styleSheet;
    }

		public function set styleSheet(value:StyleSheet):void 
    {
    	this._textfield.styleSheet = value;
    }

    public function get text():String 
    {
			return this._textfield.htmlText;
		}

		public function set text(value:String):void 
		{
			this._textfield.htmlText = value;
		}

		public function get upper():Boolean 
		{
			return _upper;
		}

		public function set upper($uc:Boolean):void 
		{
			_upper = $uc;
		}

    public function get wordWrap():Boolean 
		{
			return this._textfield.wordWrap;
		}

    public function set wordWrap(value:Boolean):void 
    {
    	this._textfield.wordWrap = value;
    }
	}
}