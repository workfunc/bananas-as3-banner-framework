package com.workfunc.display 
{
	import com.workfunc.components.Draw;

	import flash.display.Sprite;
	import flash.display.Shape;

	public class Button extends Sprite 
	{
		private var 
			_s					:Shape = 				new Shape;

		public function Button($params:Object) 
		{
			var params:Object = {};

			params.x = $params.x;
			params.y = $params.y;
			params.width = $params.width;
			params.height = $params.height;
			($params.alpha == undefined) ? params.alpha = 1 : params.alpha = $params.alpha;
			($params.rounded == undefined) ? params.rounded = false : params.rounded = true;
			($params.color == undefined) ? params.color = 0x00FFFF : params.color = $params.color

			this.addChild(Draw.rect(params));
		}
	}
}