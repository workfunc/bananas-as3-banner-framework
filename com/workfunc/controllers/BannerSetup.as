package com.workfunc.controllers 
{
	import com.workfunc.core.StaticBase;
	import com.workfunc.core.BananaTree;
	import com.workfunc.data.Model;
	import com.workfunc.events.BananasEvent;

	import flash.text.StyleSheet;

	public class BannerSetup extends StaticBase 
	{
		public static function init($params:Object, $css:StyleSheet, $width:int, $height:int):void 
		{
			Model.css = $css;
			Model.environment = $params.env;
			try 
			{
				Model.constants = 
				{
					loopAmount: $params.banner.loopAmount, 
					border: $params.banner.border, 
					background: $params.banner.background
				}
				Model.bannerWidth = $width;
				Model.bannerHeight = $height;
				Model.data = $params.banner;
				$params = {};
				$params = null;

				dispatchEvent(new BananasEvent(BananasEvent.BANNER_DATA_READY));
			} 
			catch($e:Error) 
			{
				throw new Error("🍌 Sorry, your banner size was not found or was incorrectly configured." + $e);
			}
		}

		public static function createFrameTiming():void 
		{
			FrameRouter.autoTiming(Model.data.frames, Model.constants);
		}

		public static function createFrameReference($main:*):void 
		{
			for(var i:uint = 1; i <= Model.data.frames.length; i++) 
			{
				try 
				{
					FrameRouter.addFrame = 
					{
						id: String("frame" + i), 
						frame: $main["frame" + i], 
						type: "function"
					};
					if(i == Model.data.frames.length) 
					{
						dispatchEvent(new BananasEvent(BananasEvent.FRAME_REFERENCES_CREATED));

						break;
					}
				} 
				catch($e:Error) 
				{
					throw new Error("🍌 You're missing the required frame"+ i + " function in your main Document class.\n" + $e);
				}
			}
		}

		public static function css($css:StyleSheet):void 
		{
			Model.css = $css;

			dispatchEvent(new BananasEvent(BananasEvent.CSS_LOAD_SUCCESS));
		}
	}
}