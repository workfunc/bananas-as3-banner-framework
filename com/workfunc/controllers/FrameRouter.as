package com.workfunc.controllers 
{
	import com.workfunc.core.StaticBase;
	import com.workfunc.events.BananasEvent;
	import com.workfunc.utils.Logger;

	import flash.display.DisplayObjectContainer;
	import flash.utils.Timer;
	import flash.events.TimerEvent;

	public class FrameRouter extends StaticBase 
	{
		public static var 
			endBanner									:Boolean, 
			frameCount								:int = 										1, 
			routes										:Array = 									[], 
			currentLoop								:int = 										1, 
			loopAmount								:int = 										0, 
			frameTimeCollection				:Array = 									[],
			frameFuncCollection				:Array = 									[];

		private static var 
			_allowed									:Boolean, 
			_instance									:FrameRouter;

		public function FrameRouter() 
		{
			if(!_allowed) 
			{
				throw new Error("🍌 Use FrameRouter.route() instead of instantiating with the new keyword.\n");
			}
		}

		public static function route():FrameRouter 
		{
			if(_instance == null) 
			{
				_allowed = true;
				_instance = new FrameRouter();
				_allowed = !_allowed;

				Logger.log("🍌 FrameRouter has been instantiated. You can now create the routes between frames.\n");
			} 
			else 
			{
				throw new Error("🍌 There can only be one! FrameRouter can only be instantiated once.\n");
			}
			dispatchEvent(new BananasEvent(BananasEvent.ROUTER_READY));

			return _instance;
		}

		public static function getFrame($name:String):*  
		{
			var frame:*;

			for(var i:uint = 0; i < routes.length; i++) 
			{
				if(routes[i].id == $name) 
				{
					switch(routes[i].type) 
					{
						case "function" : 
							frame = (routes[i].frame as Function)();
							break;
						case "class" || "movieclip" || "sprite" : 
							frame = (routes[i].frame as DisplayObjectContainer);
							break;
						default : 
							frame = (routes[i].frame as Function)();
							break;
					}
				}
			}
			return frame;
		}

		public static function set addFrame($obj:Object):void 
		{
			routes.push($obj);
		}

		public static function autoTiming($frames:Array, $frameData:Object):void 
		{
			loopAmount = $frameData.loopAmount;

			for each(var frame in $frames) 
			{
				for(var j:uint = 0; j < routes.length; j++) 
				{
					var timer:Timer = new Timer(frame.time * 1000);
					var func:Function = routes[j].frame;

					frameFuncCollection.push(func);

					timer.addEventListener(TimerEvent.TIMER, advance);
				}
				frameTimeCollection.push(timer);
			}
			dispatchEvent(new BananasEvent(BananasEvent.TIMING_SETUP));
		}

		public static function advance($e:TimerEvent):void 
		{
			Logger.log("\n🍌 This is the end of frame " + frameCount + " of loop " + currentLoop + ".");
			frameTimeCollection[frameCount - 1].stop();

			if(currentLoop != loopAmount) 
			{
				if(frameCount == frameTimeCollection.length) 
				{
					Logger.log("\n🍌 > Restart. This is the end of loop " + currentLoop + ".");
					currentLoop++;
					frameFuncCollection[0]();
					frameCount = 1;
					frameTimeCollection[0].start();
				} 
				else 
				{
					frameTimeCollection[frameCount].start();
					frameFuncCollection[frameCount]();
					frameCount++;
				}
			} 
			else 
			{
				endBanner = true;

				if(frameCount == frameTimeCollection.length) 
				{
					Logger.log("\n🍌 > This is the end.");
					for each(var timer in frameTimeCollection) 
					{
						timer.stop();
						timer.reset();
						timer.removeEventListener(TimerEvent.TIMER, advance);
					}
					return;
				} 
				else 
				{
					frameTimeCollection[frameCount].start();
					frameFuncCollection[frameCount]();
					frameCount++;
				}
			}
		}
	}
}