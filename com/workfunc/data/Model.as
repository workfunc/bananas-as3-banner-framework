package com.workfunc.data 
{
	import flash.text.StyleSheet;

	public class Model 
	{
		private static var 
			_environment							:String, 
			_debug										:Boolean = 						true, 
			_css 											:StyleSheet = 				new StyleSheet, 
			_bannerWidth							:int, 
			_bannerHeight							:int, 
			_properties								:Object = 						{}, 
			_constants								:Object = 						{};

		public static function set data($data:Object):void 
		{
			_properties = $data;
		}
			
		public static function get data():Object 
		{
			return _properties;
		}

		public static function set constants($constants:Object):void 
		{
			_constants = $constants;
		}
			
		public static function get constants():Object 
		{
			return _constants;
		}

		public static function set bannerWidth($w:int):void 
		{
			_bannerWidth = $w;
		}
			
		public static function get bannerWidth():int 
		{
			return _bannerWidth;
		}

		public static function set bannerHeight($h:int):void 
		{
			_bannerHeight = $h;
		}
			
		public static function get bannerHeight():int 
		{
			return _bannerHeight;
		}

		public static function set css($css:StyleSheet):void 
		{
			_css = $css;
		}
			
		public static function get css():StyleSheet 
		{
			return _css;
		}

		public static function set environment($env:String):void 
		{
			_environment = $env;
		}
			
		public static function get environment():String 
		{
			return _environment;
		}

		public static function set debug($debug:Boolean):void 
		{
			_debug = $debug;
		}
			
		public static function get debug():Boolean 
		{
			return _debug;
		}
	}
}