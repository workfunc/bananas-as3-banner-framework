package com.workfunc.data 
{
	import com.workfunc.core.StaticBase;

	import flash.text.StyleSheet;

	public class BananaStyle extends StaticBase 
	{
		private static var 
			_styles						:Object = 							{}, 
			_customCss				:StyleSheet = 					new StyleSheet;

		public static function customCss($name:String, $type:*):void 
		{
			if($type is Array) 
			{
				var mergedStyle:Object = merge($type[0], $type[1]);
				_customCss.setStyle($name, mergedStyle);
			}
			else 
			{
				_customCss.setStyle($name, $type);
			}
		}

		private static function merge($obj0:Object, $obj1:Object):Object 
		{
			var obj:Object = {};
			
			for(var p:String in $obj0) 
			{
				obj[p] = ($obj1[p] != null) ? $obj1[p] : $obj0[p];
				//trace(p, ' : $obj0', $obj0[p], '$obj1', $obj1[p], '-> new value = ', obj[p]);
			}
			for(var r:String in $obj1) 
			{
				obj[r] = ($obj0[r] != null) ? $obj0[r] : $obj1[r];
				//trace(r, ' : $obj1', $obj1[r], '$obj0', $obj0[r], '-> new value = ', obj[r]);
			}
			return obj;
		}

		public static function get css():StyleSheet 
		{
			return _customCss;
		}

		public static function set css($styles:StyleSheet):void 
		{
			_customCss = $styles;
		}

		public static function get styles():Object 
		{
			return _styles;
		}

		public static function set styles($css:Object):void 
		{
			_styles = $css;
		}
	}
}