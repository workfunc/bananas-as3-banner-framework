package com.workfunc.events 
{
	import flash.events.Event;

	public class BananasEvent extends Event 
	{
		public static const 
			MODEL_CREATE_SUCCESS					:String = 				"Model creation has succeeded", 
			JSON_LOAD_SUCCESS							:String = 				"JSON loaded successfully", 
			CSS_LOAD_SUCCESS							:String = 				"CSS loaded successfully", 
			BANNER_DATA_READY							:String = 				"Banner's data is ready", 
			ROUTER_READY									:String = 				"Frames Router is ready", 
			FRAME_REFERENCES_CREATED			:String = 				"Frames references have been created", 
			TIMING_SETUP									:String = 				"Banner timing has been setup", 
			ASSET_LOAD_SUCCESS						:String = 				"Asset loaded successfully", 
			CREATE_NAV										:String = 				"Order to build navigation", 
			ANIMATION_COMPLETE						:String = 				"Challenge animation complete";
		
		public var 
			payload:											Object;
			
		public function BananasEvent($type:String, $payload:Object = null, $bubbles:Boolean = false, $cancelable:Boolean = false) 
		{
			super($type, $bubbles, $cancelable);
			
			if($payload) payload = $payload;
		}
		
		override public function clone():Event 
		{
			return new BananasEvent(type, payload, bubbles, cancelable);
		}
		
		override public function toString():String 
		{
			return formatToString("BananasEvent", "type", "payload", "bubbles", "cancelable", "eventPhase");
		}
	}
}
