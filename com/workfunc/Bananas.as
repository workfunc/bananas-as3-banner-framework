/*
 * @author 			Jonah Bitautas
 * @framework 	Bananas v.0.8
 * @date 				11/27/13
 */
package com.workfunc 
{
	import com.workfunc.core.BananaTree;
	import com.workfunc.display.TextLine;
	import com.workfunc.display.Container;
	import com.workfunc.display.Clip;
	import com.workfunc.display.BananaClip;
	import com.workfunc.components.Draw;

	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.external.ExternalInterface;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;

	public class Bananas extends BananaClip 
	{
		public static const  
			BANNER 							:BananaTree = 							BananaTree.bananas();

		public function Bananas($main:*, $bannerWidth:int, $bannerHeight:int, $json:Object, $css:Object, $autoFrames:Boolean = true) 
		{
			super();

			BANNER.config
			({
				main: 			$main, 
				width: 			$bannerWidth, 
				height: 		$bannerHeight, 
				json: 			$json, 
				css: 				$css, 
				autoFrames: $autoFrames
			});
		}

		public function init():void 
		{
			BANNER.clickTag.addEventListener(MouseEvent.MOUSE_DOWN, clickTagHandler);
		}

		public function clickTagHandler($e:MouseEvent):void 
		{
			BANNER.trace("\n🍌 The banner's clickTag works.");
		}

		override public function addChild(child:DisplayObject):DisplayObject 
		{
			/*
			 * TextLine
			 */
			if(child is TextLine) 
			{
				var text:TextLine = TextLine(child);
				this.addEventListener(Event.ADDED, text.afterAdd, true);

				if(text.container != null) 
				{
					return text.container.addChild(text);
				} 
				else 
				{
					return super.addChild(text);
				}
			} 
			/*
			 * Clip
			 */
			else if(child is Clip) 
			{
				var clip:Clip = Clip(child);
				this.addEventListener(Event.ADDED, clip.afterAdd, true);

				if(clip.foreground) 
				{
					return BANNER.foreground.addChild(clip);
				} 
				else if(clip.aboveTheClickTag) 
				{
					return BANNER.aboveClickTag.addChild(clip);
				} 
				else 
				{
					if(clip.container != null) 
					{
						return clip.container.addChild(clip);
					} 
					else 
					{
						return super.addChild(clip);
					}
				} 
			} 
			/*
			 * Container
			 */
			else if(child is Container) 
			{
				var contain:Container = Container(child);
				this.addEventListener(Event.ADDED, contain.afterAdd, true);

				if(contain.container != null) 
				{
					return contain.container.addChild(contain);
				} 
				else 
				{
					return super.addChild(contain);
				}
			} 
			else 
			{
				return super.addChild(child);
			}
		}
	}
}