package com.workfunc.core 
{
	import com.workfunc.data.Model;
	import com.workfunc.controllers.BannerSetup;
	import com.workfunc.events.BananasEvent;
	import com.workfunc.controllers.FrameRouter;
	import com.workfunc.display.Button;
	import com.workfunc.display.TextLine;
	import com.workfunc.display.Clip;
	import com.workfunc.display.Container;
	import com.workfunc.components.Draw;
	import com.workfunc.utils.Logger;

	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.events.Event;
	import flash.geom.Matrix;
	import flash.events.TimerEvent;

	public class BananaTree extends StaticBase 
	{
		private static var 
			_aboveClickTag						:MovieClip = 							new MovieClip, 
			_topHolder								:MovieClip = 							new MovieClip, 
			_foreground								:MovieClip = 							new MovieClip, 
			_frameRouter 							:FrameRouter, 
			_clickTag									:Button, 
			_allowed									:Boolean, 
			_autoFrames								:Boolean = 								true, 
			_instance									:BananaTree, 
			_mainStage								:MovieClip;

		public function BananaTree() 
		{
			if(!_allowed) throw new Error("🍌 Use BananaTree.bananas() instead of instantiating with the new keyword.\n");
		}

		public static function bananas():BananaTree 
		{
			if(_instance == null) 
			{
				_allowed = true;
				_instance = new BananaTree();
				_allowed = !_allowed;
				trace("🍌 The Bananas framework has been instantiated.\n");
			} 
			else 
			{
				throw new Error("🍌 There can only be one! The Bananas framework can only be instantiated once.\n");
			}
			return _instance;
		}

		public function config($params:Object):void 
		{
			($params.autoFrames == false) ? _autoFrames = $params.autoFrames : _autoFrames = _autoFrames;

			try 
			{
				this.main = $params.main;
				addEventListener(BananasEvent.BANNER_DATA_READY, _createRouter);
				BannerSetup.init($params.json, $params.css, $params.width, $params.height);
			} 
			catch($e:Error) 
			{
				throw new Error("🍌 You must define the root stage when configuring Bananas.\n" + $e);
			}
		}

		private function _createRouter($e:BananasEvent):void 
		{
			this.trace("🍌 The Model has been created.\n");
			addEventListener(BananasEvent.ROUTER_READY, _buildBannerConstants);
			this.router = FrameRouter.route();
		}

		private function _buildBannerConstants($e:BananasEvent):void 
		{
			_createStaticElements();
			this.trace("🍌 The banners static elements have been created.\n");
			addEventListener(BananasEvent.FRAME_REFERENCES_CREATED, _frameRefCreated);
			_autoFrames ? BannerSetup.createFrameReference(this.main) : _initiate();
		}

		private function _createStaticElements():void 
		{
			_topHolder.name = "TopHolder";
			_topHolder.x = _topHolder.y = 0;
			this.main.addChild(_topHolder);
			this.main.addEventListener(Event.ADDED, _forceTop, true);

			_foreground.name = "StaticElements";
			_foreground.x = _foreground.y = 0;
			_topHolder.addChild(_foreground);

			_topHolder.addChild(Draw.border(
				{
					width: Model.bannerWidth, 
					height: Model.bannerHeight
				})
			);
			_clickTag = new Button(
				{
					x: 0, 
					y: 0, 
					width: Model.bannerWidth, 
					height: Model.bannerHeight, 
					alpha: 0
				}
			);
			_topHolder.addChild(_clickTag);
			_clickTag.buttonMode = true;

			_aboveClickTag.name = "AboveClickTag";
			_aboveClickTag.x = _aboveClickTag.y = 0;
			_topHolder.addChild(_aboveClickTag);
		}

		private function _frameRefCreated($e:BananasEvent):void 
		{
			addEventListener(BananasEvent.TIMING_SETUP, _initiate);
			BannerSetup.createFrameTiming();
		}

		private function _initiate($e:BananasEvent = null):void 
		{
			try 
			{
				this.main.init();

				if(_autoFrames) 
				{
					FrameRouter.frameFuncCollection[0]();
					FrameRouter.frameTimeCollection[0].start();
				}
			} 
			catch($e:Error) 
			{
				throw new Error("\n🍌 You're missing the required init function in your main Document class OR there is an error with the code in the init function.\n" + $e);
			}
		}

		public function centerOnStage($object:DisplayObject, $centerOffset:Boolean = false):void 
		{
			if(!$centerOffset) 
				$object.x = Model.bannerWidth * 0.5 - $object.width * 0.5;
			else 
				$object.x = Model.bannerWidth * 0.5;
		}

		private function _forceTop($e:Event):void 
		{
			var root:MovieClip = this.main;

			if(root == this.topHolder.parent && root.numChildren > 0 && root.getChildIndex(this.topHolder) != root.numChildren - 1) 
			{
				root.setChildIndex(this.topHolder, root.numChildren - 1);
			}
		}

		public function find($property:String, $params:Object = null):* 
		{
			if($params != null) 
			{
				var frameNumber						:int = 				$params.frame - 1, 
						itemNum								:int = 				$params.item - 1;
			}
			var returnedValue						:*;

			switch($property) 
			{
				case "background" : 
					returnedValue = 
					{
						originValues: 
						{
							x: Model.constants.background.x, 
							y: Model.constants.background.y
						}
					};
					break;
				case "logo" : 
					returnedValue = 
					{
						originValues: 
						{
							x: Model.constants.logo.x, 
							y: Model.constants.logo.y
						}
					};
					break;
				case "constants" : 
					returnedValue = 
					{
						text: Model.data.frames[frameNumber].frametext[itemNum].text, 
						css: Model.data.frames[frameNumber].frametext[itemNum].cssClass, 
						center: ($params.center == undefined) ? $params.center = false : $params.center = true, 
						originValues: (Model.constants.frames[frameNumber].frametext[itemNum].animation.origin == undefined) ? $params.originValues = {} : $params.originValues = Model.constants.frames[frameNumber].frametext[itemNum].animation.origin, 
						startValues: (Model.constants.frames[frameNumber].frametext[itemNum].animation.start == undefined) ? $params.startValues = {} : $params.startValues = Model.constants.frames[frameNumber].frametext[itemNum].animation.start, 
						endValues: (Model.constants.frames[frameNumber].frametext[itemNum].animation.end == undefined) ? $params.endValues = {} : $params.endValues = Model.constants.frames[frameNumber].frametext[itemNum].animation.end
					};
					break;
				case "text" : 
					returnedValue = 
					{
						name: String("TextLine_frame" + int(frameNumber + 1) + "_item" + int(itemNum + 1)), 
						text: Model.data.frames[frameNumber].frametext[itemNum].text, 
						css: Model.data.frames[frameNumber].frametext[itemNum].cssClass, 
						center: ($params.center == undefined) ? $params.center = false : $params.center = true, 
						uppercase: ($params.uppercase == undefined) ? $params.uppercase = false : $params.uppercase = true, 
						originValues: (Model.data.frames[frameNumber].frametext[itemNum].animation.origin == undefined) ? $params.originValues = {} : $params.originValues = Model.data.frames[frameNumber].frametext[itemNum].animation.origin, 
						startValues: (Model.data.frames[frameNumber].frametext[itemNum].animation.start == undefined) ? $params.startValues = {} : $params.startValues = Model.data.frames[frameNumber].frametext[itemNum].animation.start, 
						endValues: (Model.data.frames[frameNumber].frametext[itemNum].animation.end == undefined) ? $params.endValues = {} : $params.endValues = Model.data.frames[frameNumber].frametext[itemNum].animation.end
					};
					break;
				case "graphics" : 
					returnedValue = 
					{
						name: (Model.data.frames[frameNumber].graphics[itemNum].name == undefined) ? $params.name = "DefaultName" : $params.name = Model.data.frames[frameNumber].graphics[itemNum].name, 
						originValues: (Model.data.frames[frameNumber].graphics[itemNum].animation.origin == undefined) ? $params.originValues = {} : $params.originValues = Model.data.frames[frameNumber].graphics[itemNum].animation.origin, 
						startValues: (Model.data.frames[frameNumber].graphics[itemNum].animation.start == undefined) ? $params.startValues = {} : $params.startValues = Model.data.frames[frameNumber].graphics[itemNum].animation.start, 
						endValues: (Model.data.frames[frameNumber].graphics[itemNum].animation.end == undefined) ? $params.endValues = {} : $params.endValues = Model.data.frames[frameNumber].graphics[itemNum].animation.end
					};
					break;
				default : 
					throw new Error("\n🍌 There is no property with the name you supplied.\n" + new Error);
					break;
			}
			return returnedValue;
		}

		public function reset():void 
		{
			FrameRouter.frameCount = FrameRouter.currentLoop = 1;
			FrameRouter.loopAmount = 0;

			for(var i:uint = 0; i < FrameRouter.frameTimeCollection.length; i++) 
			{
				FrameRouter.frameTimeCollection[i].stop();
				FrameRouter.frameTimeCollection[i].reset();
				FrameRouter.frameTimeCollection[i].removeEventListener(TimerEvent.TIMER, FrameRouter.advance);
			}
			FrameRouter.frameTimeCollection = [];
			BannerSetup.createFrameTiming();
			_createStaticElements();
		}

		public function get aboveClickTag():MovieClip 
		{
			return _aboveClickTag;
		}

		public function get main():MovieClip 
		{
			return _mainStage;
		}

		public function set main($root:MovieClip):void 
		{
			_mainStage = $root;
		}

		public function get router():FrameRouter 
		{
			return _frameRouter;
		}

		public function set router($router:FrameRouter):void 
		{
			_frameRouter = $router;
		}

		public function get clickTag():Button 
		{
			return _clickTag;
		}

		public function get topHolder():MovieClip 
		{
			return _topHolder;
		}

		public function get foreground():MovieClip 
		{
			return _foreground;
		}

		public function get model():Object 
		{
			return Model.data;
		}

		public function get end():Boolean 
		{
			return FrameRouter.endBanner;
		}

		public function get environment():String 
		{
			return Model.environment;
		}

		public function get constants():Object 
		{
			return Model.constants;
		}

		public function get frames():Array 
		{
			return Model.data.frames;
		}

		public function get width():Number 
		{
			return Model.bannerWidth;
		}

		public function get height():Number 
		{
			return Model.bannerHeight;
		}

		public function get clip():Clip 
		{
			return new Clip();
		}

		public function get container():Container 
		{
			return new Container();
		}

		public function get text():TextLine 
		{
			return new TextLine();
		}

		public function trace(...args):void 
		{
			Logger.log(args);
		}

		public function showDisplayList($display:*):void 
		{
			Logger.returnDisplayList($display);
		}
	}
}